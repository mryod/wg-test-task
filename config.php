<?php
/**
 * Main configuration file
 */

//Protection key
if(!defined('ABS_KEY'))
{
    die();
}

//DB address
define('DBSERVER','localhost');

//DB user
define('DBUSER','wg-test');

//DB Password
define('DBPASSWORD','password');

//Database name
define('DATABASE','wg-test');

//Disk sector size
define('SECTOR_SIZE','512');
