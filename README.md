# README #

Hello! This application measuares and stores statistics of local server's CPU, Memory and SDA disk load.
Also, you can monitor last 15 measures on web.

### System req ###

* Debian 8+
* Apache 2 Web server
* PHP 5.6
* MySQL (latest avaliable for Debian 8)

### How do I get set up? ###

* Copy files to your web server root directory
* Create mysql database and import 'db.sql' script
* Configure all fields in config.php (database name, server, password, and your disk's block size, usually 512)
* Add a string to your cron ('/etc/crontab'): '*/15 * * * * root php /var/www/html/cron.php'. -This will store server statistics every 15 minutes.
* Enjoy :)

### What if I want more than 15 records? ###

* Feel free to edit GET_LOAD_STAT sql quety in consts.php. If you don't want any limit on records, then just delete "LIMIT 15" from statement. 
* Please notice, that too long html page can slow down your statistics load time.

### Contacts ###

* Gleb Iodo
* +375-29-649-23-03
* yod527@gmail.com
