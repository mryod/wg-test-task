<?php
/**
 * Application constant strings. (bash commands and SQL queries).
 */

if(!defined('ABS_KEY'))
{
    die();
}

//cmd

define('CPU_USAGE_CMD','sed -n \'s/^cpu\s//p\' /proc/stat');

define('MEM_USAGE_CMD', 'free | grep Mem | awk \'{print $3/$2 * 100.0}\'');

define('DISC_USAGE_CMD','awk \'{print $3,$7}\' /sys/block/sda/stat');

define('DISC_SECTOR_SIZE','lsblk -o NAME,LOG-SeC');

//sql

define('INSERT_TIME_QRY','INSERT INTO `measure_times` (`time`) VALUES (now())');

define('GET_MEASURE_ID_QRY', 'SELECT id FROM `measure_times` ORDER BY id DESC LIMIT 1');

define('INSERT_STAT_QRY','INSERT INTO `monitoring_data` (`measure_id`, `cpu_load`,`ram_load`,`disc_read`,`disc_write`) VALUES (:id,:cpu,:mem,:hdd_r,:hdd_w)');

define('GET_LOAD_STAT', 'select `time`, `cpu_load`, `ram_load`, `disc_read`, `disc_write` from monitoring_data
join measure_times
on monitoring_data.measure_id = measure_times.id
order by `time` ASC LIMIT 15');