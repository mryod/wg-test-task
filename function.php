<?php

/**
 * Main buisness logic of application.
 * TODO: Full error handlings.
 */

 if(!defined('ABS_KEY'))
 {
     die();
 }

/**
 * Initializes mysql connection using pdo
 *
 * @return PDO database connection
 */
function wgtestInitDB()
{
    try {
        $db = new PDO('mysql:host=' . DBSERVER . ';dbname=' . DATABASE, DBUSER, DBPASSWORD, array(
            PDO::ATTR_PERSISTENT => true
        ));

    } catch (PDOException $e) {
        print "Can't connect to database!: " . $e->getMessage() . "<br/>";
        die();
    }
    return $db;
}


/**
 * Calculates CPU load in 1 second.
 * @return float CPU load percentage rounded by 2.
 */

function getCurrentCpuLoad() {
    $cpu_now = explode(' ',exec(CPU_USAGE_CMD));
    $idle = intval($cpu_now[4]);
    $total_time = getTotalCpuTime($cpu_now);
    sleep(1);

    $cpu_after_sleep = explode(' ',exec(CPU_USAGE_CMD));

    $idle_after_sleep = intval($cpu_after_sleep[4]);
    $total_after_sleep = getTotalCpuTime($cpu_after_sleep);
    $diff_idle =  $idle_after_sleep - $idle;
    $diff_total = $total_after_sleep - $total_time;

    $cpu_load = (1000 * ($diff_total-$diff_idle)/$diff_total)/10;

    return round($cpu_load,2);
}

/**
 * Calculates total CPU time (sum of all cpu timings)
 * @param array $cpu_now
 * @return int sum of $cpu_now
 */
function getTotalCpuTime($cpu_now) {

    $toltal_cpu_time = 0;
    foreach ($cpu_now as $value) {
        $toltal_cpu_time += intval($value);
    }
    return $toltal_cpu_time;
}

/**
 * @return float Busy memory in percent
 */
function getCurrentRamLoad() {

    $ram_load = exec(MEM_USAGE_CMD);
    return round($ram_load,2);
}

/**
 * Measures disc current read/write speed in mb/s.
 * @return array [0] => disk read mb/s, [1] => disk write mb/s, rounded by 2.
 */
function getCurrentDiscLoad() {

    //calculate disk usage
    $disk_io = explode(' ',exec(DISC_USAGE_CMD));
    sleep(1);
    $disk_io_after_sleep = explode(' ',exec(DISC_USAGE_CMD));
    $read_diff = intval($disk_io_after_sleep[0]) - intval($disk_io[0]);
    $write_diff = intval($disk_io_after_sleep[1]) - intval($disk_io[1]);

    $disk_io_mbs = array(
        round(($read_diff * SECTOR_SIZE/1024)/1024,2),
        round(($write_diff * SECTOR_SIZE/1024)/1024,2)
    );
    return $disk_io_mbs;
}

/**
 * Stores all statistics in mysql database.
 */
function storeServerStatistics() {
    $db = wgtestInitDB();

    $stmt = $db->prepare(INSERT_TIME_QRY);
    $stmt->execute();
    $cpu_load = getCurrentCpuLoad();
    $ram_load = getCurrentRamLoad();
    $disc_load = getCurrentDiscLoad();

    $get_time_id = $db->prepare(GET_MEASURE_ID_QRY);
    $get_time_id->execute();
    $time_id_res = $get_time_id->fetchAll(PDO::FETCH_ASSOC);
    if (!$time_id_res) {
        echo "Database_error!";
        die();
    } else {
        $time_id = $time_id_res[0]['id'];
    }
    $insert_data = $db->prepare(INSERT_STAT_QRY);
    $insert_data->bindValue(':id',$time_id,PDO::PARAM_STR);
    $insert_data->bindValue(':cpu',$cpu_load,PDO::PARAM_STR);
    $insert_data->bindValue(':mem',$ram_load,PDO::PARAM_STR);
    $insert_data->bindValue(':hdd_r',$disc_load[0],PDO::PARAM_STR);
    $insert_data->bindValue(':hdd_w',$disc_load[1],PDO::PARAM_STR);

    $insert_data->execute();
}
