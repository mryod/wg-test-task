<!doctype html>

<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Server Load Statistics</title>
</head>
<body>

    <?php
        define('ABS_KEY', true);
        include "config.php";
        include "consts.php";
        include "function.php";

        $db=wgtestInitDB();
        $frontend = $db->prepare(GET_LOAD_STAT);
        $frontend->execute();
        $load_data = $frontend->fetchAll(PDO::FETCH_ASSOC);
        if (!$load_data) {
            echo "Database error!";
            die();
        }
        $count = count($load_data);
    ?>
    <h2>Server Load Statistics</h2>
    <style type="text/css">
        .tg  {border-collapse:collapse;border-spacing:0;}
        .tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}
        .tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}
        .tg .tg-wr85{font-weight:bold;background-color:#efefef;text-align:center}
        .tg .tg-erlg{font-weight:bold;background-color:#efefef;vertical-align:top}
        .tg .tg-2sfh{font-weight:bold;background-color:#efefef}
    </style>
    <table class="tg">
        <tr>
            <th class="tg-2sfh" rowspan="2">Monitoring items</th>
            <th class="tg-wr85" colspan="<?php echo $count; ?>">Load by time</th>
        </tr>

        <tr>
            <?php for ($i=0; $i<$count; $i++) { ?>
            <?php echo "<td class=\"tg-s6z2\">" . $load_data[$i]["time"] . "</td>"; ?>
            <?php } ?>
        </tr>
        <tr>
            <td class="tg-2sfh">CPU Load (%)</td>
            <?php for ($i=0; $i<$count; $i++) { ?>
                <?php echo "<td class=\"tg-s6z2\">" . $load_data[$i]["cpu_load"] . "</td>"; ?>
            <?php } ?>
        </tr>
        <tr>
            <td class="tg-erlg">Memory Load (%)</td>
            <?php for ($i=0; $i<$count; $i++) { ?>
                <?php echo "<td class=\"tg-s6z2\">" . $load_data[$i]["ram_load"] . "</td>"; ?>
            <?php } ?>
        </tr>
        <tr>
            <td class="tg-erlg">HDD Load Read/Write (MB/s)</td>
            <?php for ($i=0; $i<$count; $i++) { ?>
                <?php echo "<td class=\"tg-s6z2\">" . $load_data[$i]["disc_read"] . "/" .  $load_data[$i]["disc_write"] ."</td>"; ?>
            <?php } ?>
        </tr>


    </table>


</body>
</html>
