<?php
/**
 * A simple script to log server's statistics via cron
 */

define('ABS_KEY', true);

include "config.php";
include "consts.php";
include "function.php";

storeServerStatistics();