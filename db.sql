CREATE TABLE measure_times
(
    id INT(11) PRIMARY KEY NOT NULL AUTO_INCREMENT,
    time TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL
);
CREATE TABLE monitoring_data
(
    id INT(11) PRIMARY KEY NOT NULL AUTO_INCREMENT,
    measure_id INT(11) NOT NULL,
    cpu_load FLOAT,
    ram_load FLOAT,
    disc_read FLOAT,
    disc_write FLOAT,
    CONSTRAINT monitoring_data_measure_times_id_fk FOREIGN KEY (measure_id) REFERENCES measure_times (id)
);
CREATE INDEX monitoring_data_measure_times_id_fk ON monitoring_data (measure_id);